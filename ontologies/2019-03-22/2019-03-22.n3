@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dc11: <http://purl.org/dc/elements/1.1/> .
@prefix ns0: <http://purl.org/vocab/vann/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix ns1: <http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix cc: <http://creativecommons.org/ns#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<https://w3id.org/nno/ontology>
  a owl:Ontology ;
  dc11:publisher <http://www.aifb.kit.edu/web/Web_Science> ;
  owl:versionInfo 1.0 ;
  dc11:creator <http://www.aifb.kit.edu/web/Anna_Nguyen>, <http://www.aifb.kit.edu/web/York_Sure-Vetter>, <http://www.aifb.kit.edu/web/Tobias_Weller> ;
  dc11:title "The Neural Network Ontology" ;
  ns0:preferredNamespaceUri "https://w3id.org/nno/ontology#" ;
  rdfs:comment "This is the Neural Network Ontology. Designed by the AIFB (http://www.aifb.kit.edu/web/Web_Science)" ;
  dc11:description "This is the Neural Network Ontology. Designed by the AIFB (http://www.aifb.kit.edu/web/Web_Science)" ;
  dc11:issued "2019-03-22" ;
  rdfs:label "The Neural Network Ontology" ;
  ns1:term_status "stable" ;
  ns0:preferredNamespacePrefix "nno" ;
  cc:licence <https://creativecommons.org/licenses/by-nc-sa/4.0/> .

cc:licence a owl:AnnotationProperty .
dc11:creator a owl:AnnotationProperty .
dc11:description a owl:AnnotationProperty .
dc11:issued a owl:AnnotationProperty .
dc11:modified a owl:AnnotationProperty .
dc11:publisher a owl:AnnotationProperty .
dc11:references a owl:AnnotationProperty .
dc11:title a owl:AnnotationProperty .
ns0:preferredNamespacePrefix a owl:AnnotationProperty .
ns0:preferredNamespaceUri a owl:AnnotationProperty .
ns1:term_status a owl:AnnotationProperty .
<https://w3id.org/nno/ontology#hasActivationFunction>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Layer> ;
  rdfs:range <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:comment "Activation function used by layer." ;
  rdfs:label "has activation function" .

<https://w3id.org/nno/ontology#hasLayer>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range <https://w3id.org/nno/ontology#Layer> ;
  rdfs:comment "Layer of the Neural Network." ;
  rdfs:label "has layer" .

<https://w3id.org/nno/ontology#hasLoss_function>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range <https://w3id.org/nno/ontology#Loss_Function> ;
  rdfs:comment "Loss function used by Neural Network." ;
  rdfs:label "has loss function" .

<https://w3id.org/nno/ontology#hasNeuron>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Layer> ;
  rdfs:range <https://w3id.org/nno/ontology#Neuron> ;
  rdfs:comment "Neuron in the respective layer." ;
  rdfs:label "has neuron" .

<https://w3id.org/nno/ontology#hasOptimizer>
  a owl:ObjectProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:comment "Optimization function used by Neural Network (e.g. SGD, Adam or RMSprop)." ;
  rdfs:label "has optimizer" .

owl:topDataProperty
  rdfs:domain <https://w3id.org/nno/ontology#Layer> ;
  rdfs:range xsd:nonNegativeInteger .

<https://w3id.org/nno/ontology#dataset>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:string ;
  rdfs:comment "Describes the used dataset for training and evaluating" ;
  rdfs:label "data set" .

<https://w3id.org/nno/ontology#evaluationData>
  a owl:DatatypeProperty ;
  rdfs:subPropertyOf <https://w3id.org/nno/ontology#dataset> ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:string ;
  rdfs:comment "Dataset for evaluating the trained model. Can be the same like 'trainingdata'." ;
  rdfs:label "evaluation data" .

<https://w3id.org/nno/ontology#hasDownloadCount>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "Number of downloads for this particular model." ;
  rdfs:label "has download count" .

<https://w3id.org/nno/ontology#hasLayerSequence>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Layer> ;
  rdfs:range xsd:int ;
  rdfs:comment "Specifies the sequence of the layers. Starts at 1, the number of layers is n-1, at n: Number of layers." ;
  rdfs:label "has layer sequence" .

<https://w3id.org/nno/ontology#hasMetric>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:string ;
  rdfs:comment """Reflects potential real-world impact of the Model.
Determines the Accuracy, Precision etc. of the model.""" ;
  rdfs:label "has metric" .

<https://w3id.org/nno/ontology#hasNeurons>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Layer> ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "Describes the number of neurons that the layers provide." ;
  rdfs:label "has neurons" .

<https://w3id.org/nno/ontology#hasRepositoryLink>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:anyURI ;
  rdfs:comment "Link to the respository (e.g. github) where the model and further information can be found." ;
  rdfs:label "has repository link" .

<https://w3id.org/nno/ontology#hasintendedUse>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:string ;
  rdfs:comment "Primary intended use and users (domain) for which the Neural Network was trained for." ;
  rdfs:label "has intended use" .

<https://w3id.org/nno/ontology#trainingData>
  a owl:DatatypeProperty ;
  rdfs:subPropertyOf <https://w3id.org/nno/ontology#dataset> ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:string ;
  rdfs:comment "Dataset for training the model." ;
  rdfs:label "training data" .

<https://w3id.org/nno/ontology#usedFramework>
  a owl:DatatypeProperty ;
  rdfs:domain <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:range xsd:string ;
  rdfs:comment "Framework (e.g. Keras, Pandas, numpy, etc.) used to create this model." ;
  rdfs:label "used framework" .

foaf:Organization
  a owl:Class ;
  rdfs:comment "Organization the person belongs to." .

foaf:Person
  a owl:Class ;
  rdfs:comment "Person who makes this ontology." .

<https://w3id.org/nno/ontology#Activation>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Applies an activation function to an output." ;
  rdfs:label "Activation Layer" .

<https://w3id.org/nno/ontology#Activation_Function>
  a owl:Class ;
  rdfs:comment "Activation function used by layer." ;
  rdfs:label "Activation Function" ;
  rdfs:seeAlso "https://keras.io/activations/" .

<https://w3id.org/nno/ontology#ActivityRegularization>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Layer that applies an update to the cost function based input activity." ;
  rdfs:label "Activity Regularization Layer" .

<https://w3id.org/nno/ontology#AveragePooling1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Average pooling for temporal data." ;
  rdfs:label "Average Pooling 1D Layer" .

<https://w3id.org/nno/ontology#AveragePooling2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Average pooling operation for spatial data." ;
  rdfs:label "Average Pooling 2D Layer" .

<https://w3id.org/nno/ontology#Average_Pooling_3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Average pooling operation for 3D data (spatial or spatio-temporal)." ;
  rdfs:label "Average Pooling 3D Layer" .

<https://w3id.org/nno/ontology#BatchNormalization>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Normalization_Layer> ;
  rdfs:comment """Batch normalization layer (Ioffe and Szegedy, 2014).

Normalize the activations of the previous layer at each batch, i.e. applies a transformation that maintains the mean activation close to 0 and the activation standard deviation close to 1.""" ;
  rdfs:label "Batch Normalization Layer" .

<https://w3id.org/nno/ontology#Classification_Loss>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Loss_Function> ;
  rdfs:comment "Loss function typically used for classification problems." ;
  rdfs:label "Classification Loss Function" .

<https://w3id.org/nno/ontology#Conv1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """1D convolution layer (e.g. temporal convolution).

This layer creates a convolution kernel that is convolved with the layer input over a single spatial (or temporal) dimension to produce a tensor of outputs.""" ;
  rdfs:label "Convolutional 1D Layer" .

<https://w3id.org/nno/ontology#Conv2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """2D convolution layer (e.g. spatial convolution over images).

This layer creates a convolution kernel that is convolved with the layer input to produce a tensor of outputs.""" ;
  rdfs:label "Convolutional 2D Layer" .

<https://w3id.org/nno/ontology#Conv2DTranspose>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Transposed convolution layer (sometimes called Deconvolution).

The need for transposed convolutions generally arises from the desire to use a transformation going in the opposite direction of a normal convolution, i.e., from something that has the shape of the output of some convolution to something that has the shape of its input while maintaining a connectivity pattern that is compatible with said convolution.""" ;
  rdfs:label "Convolutional 2D Transpose Layer" .

<https://w3id.org/nno/ontology#Conv3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """3D convolution layer (e.g. spatial convolution over volumes).

This layer creates a convolution kernel that is convolved with the layer input to produce a tensor of outputs.""" ;
  rdfs:label "Convolutional 3D Layer" .

<https://w3id.org/nno/ontology#Conv3DTranspose>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Transposed convolution layer (sometimes called Deconvolution).

The need for transposed convolutions generally arises from the desire to use a transformation going in the opposite direction of a normal convolution, i.e., from something that has the shape of the output of some convolution to something that has the shape of its input while maintaining a connectivity pattern that is compatible with said convolution.""" ;
  rdfs:label "Convolutional 3D Transpose Layer" .

<https://w3id.org/nno/ontology#ConvLSTM2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment """Convolutional LSTM.

It is similar to an LSTM layer, but the input transformations and recurrent transformations are both convolutional.""" ;
  rdfs:label "Convolutional LSTM 2D Layer" .

<https://w3id.org/nno/ontology#ConvLSTM2DCell>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment "Cell class for the ConvLSTM2D layer." ;
  rdfs:label "Convolutional LSTM 2D Cell Layer" .

<https://w3id.org/nno/ontology#Convolutional_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Convolutional Layer" ;
  rdfs:seeAlso "https://keras.io/layers/convolutional/" .

<https://w3id.org/nno/ontology#Convolutional_Neural_Network>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:comment "This Class represents all Convolutional Neural Networks." ;
  rdfs:label "Convolutional Neural Network" .

<https://w3id.org/nno/ontology#Core_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Core Layer" ;
  rdfs:seeAlso "https://keras.io/layers/core/" .

<https://w3id.org/nno/ontology#Cropping1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Cropping layer for 1D input (e.g. temporal sequence).

It crops along the time dimension (axis 1).""" ;
  rdfs:label "Cropping 1D Layer" .

<https://w3id.org/nno/ontology#Cropping2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Cropping layer for 2D input (e.g. picture).

It crops along spatial dimensions, i.e. height and width.""" ;
  rdfs:label "Cropping 2D Layer" .

<https://w3id.org/nno/ontology#Cropping3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment "Cropping layer for 3D data (e.g. spatial or spatio-temporal)." ;
  rdfs:label "Cropping 3D Layer" .

<https://w3id.org/nno/ontology#CuDNNGRU>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment """Fast GRU implementation backed by CuDNN.

Can only be run on GPU, with the TensorFlow backend.""" ;
  rdfs:label "CuDNN GRU Layer" ;
  rdfs:seeAlso "https://developer.nvidia.com/cudnn" .

<https://w3id.org/nno/ontology#CuDNNLSTM>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment """Fast LSTM implementation with CuDNN.

Can only be run on GPU, with the TensorFlow backend.""" ;
  rdfs:label "CuDNN LSTM Layer" ;
  rdfs:seeAlso "https://developer.nvidia.com/cudnn" .

<https://w3id.org/nno/ontology#Dense>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Just your regular densely-connected NN layer." ;
  rdfs:label "Dense Layer" .

<https://w3id.org/nno/ontology#DepthwiseConv2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Depthwise separable 2D convolution.

Depthwise Separable convolutions consists in performing just the first step in a depthwise spatial convolution (which acts on each input channel separately).""" ;
  rdfs:label "Depthwise Convolutional 2D Layer" .

<https://w3id.org/nno/ontology#Dropout>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Applies Dropout to the input." ;
  rdfs:label "Dropout Layer" .

<https://w3id.org/nno/ontology#Embedding>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Embedding_Layer> ;
  rdfs:comment "Turns positive integers (indexes) into dense vectors of fixed size." ;
  rdfs:label "Embedding Layer" .

<https://w3id.org/nno/ontology#Embedding_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Embedding Layer" ;
  rdfs:seeAlso "https://keras.io/layers/embeddings/" .

<https://w3id.org/nno/ontology#Feed_Forward_Neural_Network>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:comment "This Class represents all Feed Forward Neural Networks." ;
  rdfs:label "Feed Forward Neural Network" .

<https://w3id.org/nno/ontology#Flatten>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Flattens the input. Does not affect the batch size." ;
  rdfs:label "Flatten Layer" .

<https://w3id.org/nno/ontology#GRU>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment """Gated Recurrent Unit - Cho et al. 2014.

There are two variants. The default one is based on 1406.1078v3 and has reset gate applied to hidden state before matrix multiplication. The other one is based on original 1406.1078v1 and has the order reversed.""" ;
  rdfs:label "GRU Layer" .

<https://w3id.org/nno/ontology#GRUCell>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment "Cell class for the GRU layer." ;
  rdfs:label "GRU Cell Layer" .

<https://w3id.org/nno/ontology#GlobalAveragePooling1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Global average pooling operation for temporal data." ;
  rdfs:label "Global Average Pooling 1D Layer" .

<https://w3id.org/nno/ontology#GlobalAveragePooling2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Global average pooling operation for spatial data." ;
  rdfs:label "Global Average Pooling 2D Layer" .

<https://w3id.org/nno/ontology#GlobalAveragePooling3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Global Average pooling operation for 3D data." ;
  rdfs:label "Global Average Pooling 3D Layer" .

<https://w3id.org/nno/ontology#GlobalMaxPooling1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Global max pooling operation for temporal data." ;
  rdfs:label "Global Max Pooling 1D Layer" .

<https://w3id.org/nno/ontology#GlobalMaxPooling2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Global max pooling operation for spatial data." ;
  rdfs:label "Global Max Pooling 2D Layer" .

<https://w3id.org/nno/ontology#GlobalMaxPooling3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Global Max pooling operation for 3D data." ;
  rdfs:label "Global Max Pooling 3D Layer" .

<https://w3id.org/nno/ontology#Input>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment """Input() is used to instantiate a Keras tensor.

A Keras tensor is a tensor object from the underlying backend (Theano, TensorFlow or CNTK), which we augment with certain attributes that allow us to build a Keras model just by knowing the inputs and outputs of the model.""" ;
  rdfs:label "Input Layer" .

<https://w3id.org/nno/ontology#InputLayer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:comment "Input (e.g. data set)." ;
  rdfs:label "Input Layer" .

<https://w3id.org/nno/ontology#LSTM>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment "Long Short-Term Memory layer - Hochreiter 1997." ;
  rdfs:label "LSTM Layer" .

<https://w3id.org/nno/ontology#LSTMCell>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:label "Cell class for the LSTM layer.", "LSTM Cell Layer" .

<https://w3id.org/nno/ontology#Lambda>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Wraps arbitrary expression as a Layer object." ;
  rdfs:label "Lambda Layer" .

<https://w3id.org/nno/ontology#Layer>
  a owl:Class ;
  rdfs:comment "Layer of the neural network." ;
  rdfs:label "Layer" .

<https://w3id.org/nno/ontology#Locally-connected_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Locally-connected Layer" ;
  rdfs:seeAlso "https://keras.io/layers/local/" .

<https://w3id.org/nno/ontology#LocallyConnected1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Locally-connected_Layer> ;
  rdfs:comment "Locally-connected layer for 1D inputs." ;
  rdfs:label "Locally-connected 1D Layer" .

<https://w3id.org/nno/ontology#LocallyConnected2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Locally-connected_Layer> ;
  rdfs:comment "Locally-connected layer for 2D inputs." ;
  rdfs:label "Locally-connected 2D Layer" .

<https://w3id.org/nno/ontology#Loss_Function>
  a owl:Class ;
  rdfs:comment "Loss function used by Neural Network." ;
  rdfs:label "Loss Function" .

<https://w3id.org/nno/ontology#Masking>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Masks a sequence by using a mask value to skip timesteps." ;
  rdfs:label "Masking Layer" .

<https://w3id.org/nno/ontology#MaxPooling1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Max pooling operation for temporal data." ;
  rdfs:label "Max Pooling 1D Layer" .

<https://w3id.org/nno/ontology#MaxPooling2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Max pooling operation for spatial data." ;
  rdfs:label "Max Pooling 2D Layer" .

<https://w3id.org/nno/ontology#MaxPooling3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Pooling_Layer> ;
  rdfs:comment "Max pooling operation for 3D data (spatial or spatio-temporal)." ;
  rdfs:label "Max Pooling 3D Layer" .

<https://w3id.org/nno/ontology#Neural_Network>
  a owl:Class ;
  rdfs:label "Neural Network" .

<https://w3id.org/nno/ontology#Neuron>
  a owl:Class ;
  rdfs:comment "Neuron of the layer." ;
  rdfs:label "Neuron" .

<https://w3id.org/nno/ontology#Normalization_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Normalization Layer" ;
  rdfs:seeAlso "https://keras.io/layers/normalization/" .

<https://w3id.org/nno/ontology#Optimizer>
  a owl:Class ;
  rdfs:comment "Optimizer used by Neural Network." ;
  rdfs:label "Optimizer" .

<https://w3id.org/nno/ontology#Permute>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment """Permutes the dimensions of the input according to a given pattern.

Useful for e.g. connecting RNNs and convnets together.""" ;
  rdfs:label "Permute Layer" .

<https://w3id.org/nno/ontology#Pooling_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Pooling Layer" ;
  rdfs:seeAlso "https://keras.io/layers/pooling/" .

<https://w3id.org/nno/ontology#RNN>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> .

<https://w3id.org/nno/ontology#Recurrent_Layer>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Layer> ;
  rdfs:label "Recurrent Layer" ;
  rdfs:seeAlso "https://keras.io/layers/recurrent/" .

<https://w3id.org/nno/ontology#Recurrent_Neural_Network>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Neural_Network> ;
  rdfs:comment "This Class represents all Recurrent Neural Networks." ;
  rdfs:label "Recurrent Neural Network" .

<https://w3id.org/nno/ontology#Regressive_Loss>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Loss_Function> ;
  rdfs:comment "Loss function typically used for regression problems." ;
  rdfs:label "Regression Loss Function" .

<https://w3id.org/nno/ontology#RepeatVector>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Repeats the input n times." ;
  rdfs:label "Repeat Vector Layer" .

<https://w3id.org/nno/ontology#Reshape>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment "Reshapes an output to a certain shape." ;
  rdfs:label "Reshape Layer" .

<https://w3id.org/nno/ontology#SeparableConv1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Depthwise separable 1D convolution.

Separable convolutions consist in first performing a depthwise spatial convolution (which acts on each input channel separately) followed by a pointwise convolution which mixes together the resulting output channels.

Intuitively, separable convolutions can be understood as a way to factorize a convolution kernel into two smaller kernels, or as an extreme version of an Inception block.""" ;
  rdfs:label "Separable Convolutional 1D Layer" .

<https://w3id.org/nno/ontology#SeparableConv2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Depthwise separable 2D convolution.

Separable convolutions consist in first performing a depthwise spatial convolution (which acts on each input channel separately) followed by a pointwise convolution which mixes together the resulting output channels.

Intuitively, separable convolutions can be understood as a way to factorize a convolution kernel into two smaller kernels, or as an extreme version of an Inception block.""" ;
  rdfs:label "Separable Convolutional 2D Layer" .

<https://w3id.org/nno/ontology#SimpleRNN>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment "Fully-connected RNN where the output is to be fed back to input." ;
  rdfs:label "Simple RNN Layer" .

<https://w3id.org/nno/ontology#SimpleRNNCell>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Recurrent_Layer> ;
  rdfs:comment "Cell class for SimpleRNN." ;
  rdfs:label "Simple RNN Cell Layer" .

<https://w3id.org/nno/ontology#SpatialDropout1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment """Spatial 1D version of Dropout.

This version performs the same function as Dropout, however it drops entire 1D feature maps instead of individual elements. If adjacent frames within feature maps are strongly correlated (as is normally the case in early convolution layers) then regular dropout will not regularize the activations and will otherwise just result in an effective learning rate decrease. In this case, SpatialDropout1D will help promote independence between feature maps and should be used instead.""" ;
  rdfs:label "Spatial Dropout 1D Layer" .

<https://w3id.org/nno/ontology#SpatialDropout2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment """Spatial 2D version of Dropout.

This version performs the same function as Dropout, however it drops entire 2D feature maps instead of individual elements. If adjacent pixels within feature maps are strongly correlated (as is normally the case in early convolution layers) then regular dropout will not regularize the activations and will otherwise just result in an effective learning rate decrease. In this case, SpatialDropout2D will help promote independence between feature maps and should be used instead.""" ;
  rdfs:label "Spatial Dropout 2D Layer" .

<https://w3id.org/nno/ontology#SpatialDropout3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Core_Layer> ;
  rdfs:comment """Spatial 3D version of Dropout.

This version performs the same function as Dropout, however it drops entire 3D feature maps instead of individual elements. If adjacent pixels within feature maps are strongly correlated (as is normally the case in early convolution layers) then regular dropout will not regularize the activations and will otherwise just result in an effective learning rate decrease. In this case, SpatialDropout3D will help promote independence between feature maps and should be used instead.""" ;
  rdfs:label "Spatial Dropout 3D Layer" .

<https://w3id.org/nno/ontology#UpSampling1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment "Upsampling layer for 1D inputs." ;
  rdfs:label "Upsampling 1D Layer" .

<https://w3id.org/nno/ontology#UpSampling2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Upsampling layer for 2D inputs.

Repeats the rows and columns of the data by size[0] and size[1] respectively.""" ;
  rdfs:label "Upsampling 2D Layer" .

<https://w3id.org/nno/ontology#UpSampling3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Upsampling layer for 3D inputs.

Repeats the 1st, 2nd and 3rd dimensions of the data by size[0], size[1] and size[2] respectively.""" ;
  rdfs:label "Upsampling 3D Layer" .

<https://w3id.org/nno/ontology#ZeroPadding1D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment "Zero-padding layer for 1D input (e.g. temporal sequence)." ;
  rdfs:label "Zero-padding 1D Layer" .

<https://w3id.org/nno/ontology#ZeroPadding2D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment """Zero-padding layer for 2D input (e.g. picture).

This layer can add rows and columns of zeros at the top, bottom, left and right side of an image tensor.""" ;
  rdfs:label "Zero-padding 2D Layer" .

<https://w3id.org/nno/ontology#ZeroPadding3D>
  a owl:Class ;
  rdfs:subClassOf <https://w3id.org/nno/ontology#Convolutional_Layer> ;
  rdfs:comment "Zero-padding layer for 3D data (spatial or spatio-temporal)." ;
  rdfs:label "Zero-padding 3D Layer" .

<http://www.aifb.kit.edu/web/Anna_Nguyen>
  a owl:NamedIndividual, foaf:Person ;
  rdfs:label "Anna Nguyen" ;
  rdfs:seeAlso "http://www.aifb.kit.edu/web/Anna_Nguyen" .

<http://www.aifb.kit.edu/web/Tobias_Weller>
  a owl:NamedIndividual, foaf:Person ;
  rdfs:label "Tobias Weller" ;
  rdfs:seeAlso "http://www.aifb.kit.edu/web/Tobias_Weller" .

<http://www.aifb.kit.edu/web/Web_Science>
  a owl:NamedIndividual, foaf:Organization ;
  rdfs:label "Web Science (AIFB)" ;
  rdfs:seeAlso "http://www.aifb.kit.edu/web/Web_Science" .

<http://www.aifb.kit.edu/web/York_Sure-Vetter>
  a owl:NamedIndividual, foaf:Person ;
  rdfs:label "York Sure-Vetter" ;
  rdfs:seeAlso "http://www.aifb.kit.edu/web/York_Sure-Vetter" .

<https://w3id.org/nno/ontology#adadelta>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:comment "Adadelta is a more robust extension of Adagrad that adapts learning rates based on a moving window of gradient updates, instead of accumulating all past gradients. This way, Adadelta continues learning even when many updates have been done. Compared to Adagrad, in the original version of Adadelta you don't have to set an initial learning rate. In this version, initial learning rate and decay factor can be set, as in most other Keras optimizers." ;
  rdfs:label "Adadelta Optimizer" .

<https://w3id.org/nno/ontology#adagrad>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:comment "Adagrad is an optimizer with parameter-specific learning rates, which are adapted relative to how frequently a parameter gets updated during training. The more updates a parameter receives, the smaller the learning rate." ;
  rdfs:label "Adagrad Optimizer" .

<https://w3id.org/nno/ontology#adam>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:label "Adam Optimizer" .

<https://w3id.org/nno/ontology#adamax>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:label "Adamax Optimizer" .

<https://w3id.org/nno/ontology#add>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:comment "Functional interface to the Add layer." ;
  rdfs:label "Add" .

<https://w3id.org/nno/ontology#biasadd>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:comment "Adds a bias vector to a tensor." ;
  rdfs:label "Bias Add" .

<https://w3id.org/nno/ontology#binary_crossentropy>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Binary Crossentropy" .

<https://w3id.org/nno/ontology#categorical_crossentropy>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Categorical Crossentropy" .

<https://w3id.org/nno/ontology#categorical_hinge>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Categorical Hinge" .

<https://w3id.org/nno/ontology#cosine_proximity>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Cosine Proximity" .

<https://w3id.org/nno/ontology#elu>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Exponential Linear Unit" .

<https://w3id.org/nno/ontology#exponential>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Exponential" .

<https://w3id.org/nno/ontology#hard_sigmoid>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Hard Sigmoid" .

<https://w3id.org/nno/ontology#hinge>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Hinge" .

<https://w3id.org/nno/ontology#identity>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:comment "Returns a tensor with the same content as the input tensor." ;
  rdfs:label "Identity" .

<https://w3id.org/nno/ontology#kullback_leibler_divergence>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Kullback Leibler Divergence" .

<https://w3id.org/nno/ontology#linear>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Linear" .

<https://w3id.org/nno/ontology#logcosh>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Log Cosh Loss" .

<https://w3id.org/nno/ontology#maxpool>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Maxpool" .

<https://w3id.org/nno/ontology#mean_absolute_error>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Mean Absolute Error" .

<https://w3id.org/nno/ontology#mean_absolute_percentage_error>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Mean Absolute Percentage Error" .

<https://w3id.org/nno/ontology#mean_squared_error>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Mean Squared Error" .

<https://w3id.org/nno/ontology#mean_squared_logarithmic_error>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Mean Squared Logarithmic Error" .

<https://w3id.org/nno/ontology#merge>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Merge" .

<https://w3id.org/nno/ontology#nadam>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:comment "Much like Adam is essentially RMSprop with momentum, Nadam is Adam RMSprop with Nesterov momentum." ;
  rdfs:label "Nesterov Adam Optimizer" .

<https://w3id.org/nno/ontology#poisson>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Regressive_Loss> ;
  rdfs:label "Poisson" .

<https://w3id.org/nno/ontology#relu>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Rectified Linear Unit" .

<https://w3id.org/nno/ontology#reshape>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:comment "Reshapes a tensor to the specified shape." ;
  rdfs:label "Reshape" .

<https://w3id.org/nno/ontology#resizenearestneighbor>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Resize Nearest Neighbor" .

<https://w3id.org/nno/ontology#rmsprop>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:comment "This optimizer is usually a good choice for recurrent neural networks." ;
  rdfs:label "RMSProp Optimizer" .

<https://w3id.org/nno/ontology#selu>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Scaled Exponential Linear Unit" .

<https://w3id.org/nno/ontology#sgd>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Optimizer> ;
  rdfs:comment "Includes support for momentum, learning rate decay, and Nesterov momentum." ;
  rdfs:label "Stochastic Gradient Descent Optimizer" .

<https://w3id.org/nno/ontology#sigmoid>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Sigmoid" .

<https://w3id.org/nno/ontology#softmax>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Softmax" .

<https://w3id.org/nno/ontology#softplus>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Softplus" .

<https://w3id.org/nno/ontology#softsign>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Softsign" .

<https://w3id.org/nno/ontology#sparse_categorical_crossentropy>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Sparse Categorical Crossentropy" .

<https://w3id.org/nno/ontology#squared_hinge>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Classification_Loss> ;
  rdfs:label "Squared Hinge" .

<https://w3id.org/nno/ontology#tanh>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Hyperbolic Tangent" .

<https://w3id.org/nno/ontology#tensorarrayreadv3>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:label "Tensor Array Read V3" .

<https://w3id.org/nno/ontology#transpose>
  a owl:NamedIndividual, <https://w3id.org/nno/ontology#Activation_Function> ;
  rdfs:comment "Transposes a tensor and returns it." ;
  rdfs:label "Transpose" .

