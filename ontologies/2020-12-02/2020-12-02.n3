@prefix :      <http://www.w3id.org/urban-iot/core#> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml:   <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .

<http://www.w3.org/ns/sosa/observedProperty>
        a           owl:ObjectProperty ;
        rdfs:label  "proprietà campionata"@it , "observed property"@en .

<http://schema.org/email>
        a           owl:DatatypeProperty ;
        rdfs:label  "email"@it , "email"@en .

<http://schema.org/priceCurrency>
        a           owl:DatatypeProperty ;
        rdfs:label  "valuta del prezzo"@it , "price currency"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Valori secondo lo standard ISO 4217 (es. \"EUR\" per l'Euro)."@it .

<http://www.w3.org/2006/time#timeZone>
        a           owl:ObjectProperty ;
        rdfs:label  "time zone"@en , "fuso orario"@it .

<http://schema.org/itemCondition>
        a           owl:DatatypeProperty ;
        rdfs:label  "item condition"@en , "condizioni dell'oggetto"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare una schema:Offer (Offerta) ad una descrizione testuale (xsd:string) dei criteri di applicazione, calcolo del costo, periodo di validità e informazioni addizionali."@it .

<http://www.w3.org/ns/sosa/madeBySensor>
        a           owl:ObjectProperty ;
        rdfs:label  "made by sensor"@en , "eseguita dal sensore"@it .

:serialNumber  a      owl:DatatypeProperty ;
        rdfs:comment  "Indicazione del numero seriale."@it , "Indication of the serial number."@en ;
        rdfs:domain   :ServiceResource ;
        rdfs:label    "serial number"@en , "numero seriale"@it ;
        rdfs:range    xsd:string .

<http://schema.org/opens>
        a           owl:DatatypeProperty ;
        rdfs:label  "opens"@en , "apre"@it .

:ServiceResource  a   owl:Class ;
        rdfs:comment  "Risorsa messa a disposizione da un Servizio che può essere utilizzata in una Sessione di Utilizzo."@it , "A Resource made available by a Service to be used in a Usage Session."@en ;
        rdfs:label    "Service Resource"@en , "Risorsa del Servizio"@it .

:observationIn  a     owl:ObjectProperty ;
        rdfs:comment  "Associates an Observation with the Sensor Record containing it."@en , "Associa un Campionamento al Record del Sensore di cui fa parte."@it ;
        rdfs:domain   <http://www.w3.org/ns/sosa/Observation> ;
        rdfs:label    "observation in"@en , "campionamento in"@it ;
        rdfs:range    :SensorRecord .

:recordRegisteredBy  a  owl:ObjectProperty ;
        rdfs:comment   "Associates a Sensor Record with the Sensor registering it."@en , "Associa un Record del Sensore al Sensore che lo ha registrato."@it ;
        rdfs:domain    :SensorRecord ;
        rdfs:label     "record registrato da"@it , "record registered by"@en ;
        rdfs:range     <http://www.w3.org/ns/sosa/Sensor> ;
        owl:inverseOf  :registersRecord .

<http://schema.org/Offer>
        a           owl:Class ;
        rdfs:label  "Offerta"@it , "Offer"@en .

<http://www.w3.org/ns/locn#fullAddress>
        a           owl:DatatypeProperty ;
        rdfs:label  "indirizzo completo"@it , "full address"@en .

:registersRecord  a   owl:ObjectProperty ;
        rdfs:comment  "Associates a Sensor with a registered Sensor Record."@en , "Associa un Sensore ad un Record del Sensore."@it ;
        rdfs:domain   <http://www.w3.org/ns/sosa/Sensor> ;
        rdfs:label    "registra record"@it , "registers record"@en ;
        rdfs:range    :SensorRecord .

<http://www.w3id.org/urban-iot/core>
        a                owl:Ontology ;
        rdfs:comment     "Core module of the suite of ontologies for urban IoT devices."@en , "Modulo core delle ontologie per gli apparati IoT urbani."@it ;
        rdfs:label       "Urban IoT Ontologies - Core Module"@en , "Ontologie per gli Apparati IoT Urbani - Modulo Core"@it ;
        <http://purl.org/dc/elements/1.1/contributor>
                "Direzione Mobilità e Trasporti - Area Pianificazione e Programmazione Mobilità (Comune di Milano)" , "Direzione Sistemi Informativi ed Agenda Digitale - Area Gestione ed Integrazione Dati (Comune di Milano)" , "Area Pianificazione e Monitoraggio (AMAT Milano)" ;
        <http://purl.org/dc/elements/1.1/creator>
                "Irene Celino (Cefriel)" , "Mario Scrocca (Cefriel)" , "Ilaria Baroni (Cefriel)" ;
        <http://purl.org/dc/elements/1.1/publisher>
                "Comune di Milano" ;
        <http://purl.org/dc/elements/1.1/rights>
                "Copyright Comune di Milano, 2020" ;
        <http://purl.org/dc/elements/1.1/title>
                "Ontologie per gli Apparati IoT Urbani - Modulo Core"@it , "Urban IoT Ontologies - Core Module"@en ;
        <http://purl.org/dc/terms/bibliographicCitation>
                "Comune di Milano. Urban IoT Ontologies - Core Module (v1.0.0). http://www.w3id.org/urban-iot/core" ;
        <http://purl.org/dc/terms/license>
                <https://creativecommons.org/licenses/by/4.0/> ;
        <http://purl.org/vocab/vann/preferredNamespacePrefix>
                "uiot" ;
        <http://purl.org/vocab/vann/preferredNamespaceUri>
                "http://www.w3id.org/urban-iot/core#" ;
        owl:versionIRI   <http://www.w3id.org/urban-iot/core/1.0.0> ;
        owl:versionInfo  "v1.0.0" .

<http://purl.org/dc/terms/license>
        a       owl:AnnotationProperty .

:ServiceUser  a       owl:Class ;
        rdfs:comment  "Utente utilizzatore di un Servizio."@it , "A User using a Service."@en ;
        rdfs:label    "Utente del Servizio"@it , "Service User"@en .

<http://schema.org/Event>
        a           owl:Class ;
        rdfs:label  "Evento"@it , "Event"@en .

<http://purl.org/vocab/vann/preferredNamespaceUri>
        a       owl:AnnotationProperty .

<http://schema.org/Service>
        a           owl:Class ;
        rdfs:label  "Servizio"@it , "Service"@en .

<http://schema.org/legalName>
        a           owl:DatatypeProperty ;
        rdfs:label  "nome legale"@it , "legal name"@en .

<http://schema.org/itemOffered>
        a           owl:ObjectProperty ;
        rdfs:label  "oggetto offerto"@it , "item offered"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare una schema:Offer (Offerta) al schema:Service (Servizio) a cui è riferita."@it .

<http://www.w3.org/ns/legal#LegalEntity>
        a           owl:Class ;
        rdfs:label  "Legal Entity"@en , "Entità Legale"@it .

<http://schema.org/serviceUrl>
        a           owl:DatatypeProperty ;
        rdfs:label  "url del servizio"@it , "service url"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usato per associare uno schema:ServiceChannel (Canale del Servizio) ad un url (xsd:anyURI)."@it .

<http://schema.org/closes>
        a           owl:DatatypeProperty ;
        rdfs:label  "closes"@en , "chiude"@it .

:reservationUsed  a   owl:DatatypeProperty ;
        rdfs:comment  "Indicates whether a reservation mechanism has been used for a Usage Session."@en , "Indica se un meccanismo di prenotazione è stato utilizzato per una Sessione di Utilizzo."@it ;
        rdfs:domain   :UsageSession ;
        rdfs:label    "reservation used"@en , "prenotazione utilizzata"@it ;
        rdfs:range    xsd:boolean .

<http://schema.org/startDate>
        a           owl:DatatypeProperty ;
        rdfs:label  "start date"@en , "data di inizio"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Literal datatype xsd:dateTime."@it .

:userEnabledBy  a     owl:ObjectProperty ;
        rdfs:comment  "Associates a Service Private User to the Service Business User enabling it."@en , "Associa un Utente Privato del Servizio all'Utente Business che lo abilita."@it ;
        rdfs:domain   :ServicePrivateUser ;
        rdfs:label    "utente abilitato da"@it , "user enabled by"@en ;
        rdfs:range    :ServiceBusinessUser .

<http://schema.org/price>
        a           owl:DatatypeProperty ;
        rdfs:label  "price"@en , "prezzo"@it .

<http://schema.org/name>
        a           owl:DatatypeProperty ;
        rdfs:label  "nome"@it , "name"@en .

:hasEnabledUser  a     owl:ObjectProperty ;
        rdfs:comment   "Associates a Service Business User with the Service Private User enabled by that business user."@en , "Associa un Utente Business del Servizio ad un Utente Privato abilitato dall'utenza business."@it ;
        rdfs:domain    :ServiceBusinessUser ;
        rdfs:label     "has enabled user"@en , "ha utente abilitato"@it ;
        rdfs:range     :ServicePrivateUser ;
        owl:inverseOf  :userEnabledBy .

<http://schema.org/provider>
        a           owl:ObjectProperty ;
        rdfs:label  "provider"@en , "erogatore"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:Service (Servizio) alla schema:Organization (Organizzazione) che lo eroga."@it .

:SensorRecord  a      owl:Class ;
        rdfs:comment  "Un Record del Sensore aggrega un insieme di campionamenti associati allo stesso timestamp e allo stesso sensore."@it , "A Sensor Record aggregates a set of observations associated to the same timestamp and the same sensor."@en ;
        rdfs:label    "Sensor Record"@en , "Record del Sensore"@it .

:associatedBusiness  a  owl:ObjectProperty ;
        rdfs:comment  "Associates a Usage Session with the Service Business User enabling it."@en , "Associa una Sessione di Utilizzo all'Utente Business del Servizio che la abilita."@it ;
        rdfs:domain   :UsageSession ;
        rdfs:label    "business associato"@it , "associated business"@en ;
        rdfs:range    :ServiceBusinessUser .

:hasUser  a            owl:ObjectProperty ;
        rdfs:comment   "Associates a Service to a Service User."@en , "Associa un Servizio ad un Utente del Servizio."@it ;
        rdfs:domain    <http://schema.org/Service> ;
        rdfs:label     "has user"@en , "ha utente"@it ;
        rdfs:range     :ServiceUser ;
        owl:inverseOf  :userOfService .

<http://www.w3.org/ns/locn#postName>
        a           owl:DatatypeProperty ;
        rdfs:label  "post name"@en , "divisione postale"@it .

<http://www.w3.org/ns/sosa/hasResult>
        a           owl:ObjectProperty ;
        rdfs:label  "has result"@en , "ha risultato"@it .

<http://purl.org/vocab/vann/usageNote>
        a       owl:AnnotationProperty .

<http://schema.org/gender>
        a           owl:DatatypeProperty ;
        rdfs:label  "genere"@it , "gender"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Literal datatype xsd:string."@it .

<http://purl.org/dc/elements/1.1/publisher>
        a       owl:AnnotationProperty .

<http://purl.org/dc/elements/1.1/creator>
        a       owl:AnnotationProperty .

:UsageSession  a         owl:Class ;
        rdfs:comment     "Una Sessione di Utilizzo è un Evento associato ad un Servizio e ad una Risorsa del Servizio che rappresenta un utilizzo della risorsa resa disponibile dal Servizio."@it , "A Usage Session is an Event associated to a Service and a Service Resource representing a usage of the resource made available by the Service."@en ;
        rdfs:label       "Usage Session"@en , "Sessione di Utilizzo"@it ;
        rdfs:subClassOf  <http://schema.org/Event> .

:sessionPerformedBy  a  owl:ObjectProperty ;
        rdfs:comment  "Associates a Usage Session with the Service Private User performing it."@en , "Associa una Sessione di Utilizzo all'Utente Privato del Servizio che la effettua."@it ;
        rdfs:domain   :UsageSession ;
        rdfs:label    "sessione effettuata da"@it , "session performed by"@en ;
        rdfs:range    :ServicePrivateUser .

<http://schema.org/duration>
        a           owl:DatatypeProperty ;
        rdfs:label  "duration"@en , "durata"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Literal consigliato xsd:duration."@it .

:userOfService  a     owl:ObjectProperty ;
        rdfs:comment  "Associates a Service User to the Service used."@en , "Associa un Utente del Servizio al Servizio che utilizza."@it ;
        rdfs:domain   :ServiceUser ;
        rdfs:label    "utente del servizio"@it , "user of service"@en ;
        rdfs:range    <http://schema.org/Service> .

:recordTimestamp  a   owl:DatatypeProperty ;
        rdfs:comment  "Timestamp associato ad un Record del Sensore."@it , "Timestamp associated with a Sensor Record."@en ;
        rdfs:domain   :SensorRecord ;
        rdfs:label    "timestamp del record"@it , "record timestamp"@en ;
        rdfs:range    xsd:dateTime .

<http://schema.org/description>
        a           owl:DatatypeProperty ;
        rdfs:label  "descrizione"@it , "description"@en .

<http://schema.org/DayOfWeek>
        a           owl:Class ;
        rdfs:label  "Giorno della Settimana"@it , "Day of Week"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Istanze della classe usate per definire una schema:OpeningHoursSpecification (Specifica Orari di Apertura)."@it .

<http://purl.org/dc/terms/identifier>
        a           owl:DatatypeProperty ;
        rdfs:label  "identifier"@en , "identificativo"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare un identificativo univoco ad una instanza."@it .

<http://www.w3.org/ns/sosa/Sensor>
        a           owl:Class ;
        rdfs:label  "Sensore"@it , "Sensor"@en .

<http://www.w3.org/ns/locn#Geometry>
        a           owl:Class ;
        rdfs:label  "Geometry"@en , "Geometria"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "E' consigliato di utilizzare la GeoSPARQL specification (http://www.w3.org/2011/02/GeoSPARQL.pdf) per l'encoding."@it .

<http://www.w3.org/ns/sosa/hasSimpleResult>
        a           owl:DatatypeProperty ;
        rdfs:label  "has simple result"@en , "ha valore semplice"@it .

<http://schema.org/url>
        a             owl:DatatypeProperty ;
        rdfs:comment  "Literal datatype xsd:anyURI."@en ;
        rdfs:label    "url"@it , "url"@en .

<http://www.w3.org/ns/locn#geometry>
        a           owl:ObjectProperty ;
        rdfs:label  "geometry"@en , "geometria"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Associa una dcterms:Location (Localizzazione) alla locn:Geometry (Geometria) che la descrive."@it .

<http://www.w3.org/ns/sosa/madeObservation>
        a           owl:ObjectProperty ;
        rdfs:label  "made observation"@en , "ha eseguito campionamento"@it .

:registeredTo  a      owl:ObjectProperty ;
        rdfs:comment  "Associates a Service User to the Service to which the user is registered to."@en , "Associa un Utente al Servizio a cui è registrato."@it ;
        rdfs:domain   :ServiceUser ;
        rdfs:label    "registrato a"@it , "registered to"@en ;
        rdfs:range    <http://schema.org/Service> .

<http://www.w3.org/ns/locn#address>
        a           owl:ObjectProperty ;
        rdfs:label  "indirizzo"@it , "address"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare una locn:Location (Localizzazione) o una schema:Persona (Person) al suo indirizzo (locn:Address)."@it .

<http://schema.org/ContactPoint>
        a           owl:Class ;
        rdfs:label  "Punto di Contatto"@it , "Contact Point"@en .

<http://schema.org/offers>
        a           owl:ObjectProperty ;
        rdfs:label  "offre"@it , "offers"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:Service (Servizio) alle schema:Offer (Offerta) disponibili."@it .

<http://schema.org/alternateName>
        a           owl:DatatypeProperty ;
        rdfs:label  "nome alternativo"@it , "alternate name"@en .

<http://schema.org/servicePhone>
        a           owl:ObjectProperty ;
        rdfs:label  "service phone"@en , "contatto del servizio"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:ServiceChannel (Canale del Servizio) ad uno schema:ContactPoint (Punto di Contatto)."@it .

:usesResource  a      owl:ObjectProperty ;
        rdfs:comment  "Associates a Usage Session with the Service Resource used in the session."@en , "Associa una Sessione di Utilizzo alla Risorsa del Servizio utilizzata."@it ;
        rdfs:domain   :UsageSession ;
        rdfs:label    "utilizza risorsa"@it , "uses resource"@en ;
        rdfs:range    :ServiceResource .

:registrationDate  a  owl:DatatypeProperty ;
        rdfs:comment  "Date in which a Service User registered to the Service."@en , "Data di registrazione dell'Utente del Servizio al Servizio."@it ;
        rdfs:domain   :ServiceUser ;
        rdfs:label    "registration date"@en , "data di registrazione"@it ;
        rdfs:range    xsd:dateTime .

:resourceUsedIn  a     owl:ObjectProperty ;
        rdfs:comment   "Associates a Service Resource with a Usage Session using it."@en , "Associa una Risorsa del Servizio ad una Sessione di Utilizzo in cui è stata utilizzata."@it ;
        rdfs:domain    :ServiceResource ;
        rdfs:label     "risorsa usata in"@it , "resource used in"@en ;
        rdfs:range     :UsageSession ;
        owl:inverseOf  :usesResource .

:fixedAmount  a             owl:DatatypeProperty ;
        rdfs:comment        "Indica un costo fisso dell'Offerta."@it , "Associates an Offer with a fixed amount required from it."@en ;
        rdfs:domain         <http://schema.org/Offer> ;
        rdfs:label          "fixed amount"@en , "costo fisso"@it ;
        rdfs:range          xsd:double ;
        rdfs:subPropertyOf  <http://schema.org/price> .

:containsObservation  a  owl:ObjectProperty ;
        rdfs:comment   "Associates a Sensor Record with a Observation contained in it."@en , "Associa un Record del Sensore ad un Campionamento contenuto nel record."@it ;
        rdfs:domain    :SensorRecord ;
        rdfs:label     "contiene campionamento"@it , "contains observation"@en ;
        rdfs:range     <http://www.w3.org/ns/sosa/Observation> ;
        owl:inverseOf  :observationIn .

:performsSession  a    owl:ObjectProperty ;
        rdfs:comment   "Associates a Service Private User with a Usage Session performed."@en , "Associa un Utente Privato del Servizio ad una Sessione di Utilizzo effettuata."@it ;
        rdfs:domain    :ServicePrivateUser ;
        rdfs:label     "performs session"@en , "effettua sessione"@it ;
        rdfs:range     :UsageSession ;
        owl:inverseOf  :sessionPerformedBy .

:ServiceBusinessUser  a  owl:Class ;
        rdfs:comment     "Legal Entity with a business contract for the service. A Service Business User can not directly use the service but can enable a Service Private User to do so."@en , "Entità Legale con contratto business per accedere al servizio. Un Utente Business non può utilizzare direttamente il servizio ma può abilitare un Utente Privato del Servizio a farlo."@it ;
        rdfs:label       "Utente Business del Servizio"@it , "Service Business User"@en ;
        rdfs:subClassOf  :ServiceUser , <http://www.w3.org/ns/legal#LegalEntity> .

:includedInMobilityStation
        a             owl:ObjectProperty ;
        rdfs:comment  "Associates a Mobility Station with another Mobility Station including it."@en , "Associa una Stazione di Mobilità ad una Stazione di Mobilità che la include."@it ;
        rdfs:domain   :MobilityStation ;
        rdfs:label    "inclusa nella stazione di mobilità"@it , "included in mobility station"@en ;
        rdfs:range    :MobilityStation .

<http://schema.org/Organization>
        a           owl:Class ;
        rdfs:label  "Organizzazione"@it , "Organization"@en .

<http://schema.org/OpeningHoursSpecification>
        a           owl:Class ;
        rdfs:label  "Specifica Orari di Apertura"@it , "Opening Hours Specification"@en .

<http://www.w3.org/ns/sosa/observes>
        a           owl:ObjectProperty ;
        rdfs:label  "osserva"@it , "observes"@en .

<http://www.w3.org/ns/locn#location>
        a           owl:ObjectProperty ;
        rdfs:label  "location"@en , "localizzazione"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:Place (Luogo) alla locn:Location che descrive la sua localizzazione."@it .

<http://schema.org/contactPoint>
        a           owl:ObjectProperty ;
        rdfs:label  "punto di contatto"@it , "contact point"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare una schema:Organization (Organizzazione) ad un suo schema:ContactPoint (Punto di Contatto) disponibile."@it .

:offerAssociated  a   owl:ObjectProperty ;
        rdfs:comment  "Associates a Usage Session with the tariff (Offer) applied to the specific session."@en , "Associa una Sessione di Utilizzo alla tariffa (Offerta) applicata alla specifica sessione."@it ;
        rdfs:domain   :UsageSession ;
        rdfs:label    "offerta associata"@it , "offer associated"@en ;
        rdfs:range    <http://schema.org/Offer> .

<http://schema.org/dayOfWeek>
        a           owl:ObjectProperty ;
        rdfs:label  "giorno della settimana"@it , "day of week"@en .

<http://schema.org/endDate>
        a           owl:DatatypeProperty ;
        rdfs:label  "end date"@en , "data di fine"@it .

:deregistrationDate  a  owl:DatatypeProperty ;
        rdfs:comment  "Date in which a Service User deregistered from a Service."@en , "Data in cui l'Utente del Servizio ha chiesto la cancellazione della registrazione dal Servizio."@it ;
        rdfs:domain   :ServiceUser ;
        rdfs:label    "deregistration date"@en , "data di disdetta"@it ;
        rdfs:range    xsd:dateTime .

<http://schema.org/areaServed>
        a           owl:ObjectProperty ;
        rdfs:label  "area servita"@it , "area served"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:Service (Servizio) allo schema:Place (Luogo) che è servito dal servizio e in cui il servizio è accessibile."@it .

<http://www.w3.org/ns/sosa/Observation>
        a           owl:Class ;
        rdfs:label  "Observation"@en , "Campionamento"@it .

:latestRecord  a            owl:ObjectProperty ;
        rdfs:comment        "Associates a Sensor with the latest registered Sensor Record."@en , "Associa un Sensore all'ultimo Record del Sensore registrato."@it ;
        rdfs:domain         <http://www.w3.org/ns/sosa/Sensor> ;
        rdfs:label          "ultimo record"@it , "latest record"@en ;
        rdfs:range          :SensorRecord ;
        rdfs:subPropertyOf  :registersRecord .

<http://purl.org/vocab/vann/preferredNamespacePrefix>
        a       owl:AnnotationProperty .

<http://schema.org/ServiceChannel>
        a           owl:Class ;
        rdfs:label  "Service Channel"@en , "Canale del Servizio"@it .

<http://schema.org/Person>
        a           owl:Class ;
        rdfs:label  "Persona"@it , "Person"@en .

:makesAvailable  a    owl:ObjectProperty ;
        rdfs:comment  "Associates a Service with the Service Resource that makes available."@en , "Associa un Servizio ad una risorsa che rende disponibile."@it ;
        rdfs:domain   <http://schema.org/Service> ;
        rdfs:label    "rende disponibile"@it , "makes available"@en ;
        rdfs:range    :ServiceResource .

<http://schema.org/telephone>
        a             owl:DatatypeProperty ;
        rdfs:comment  "Literal datatype xsd:string."@en ;
        rdfs:label    "telephone"@en , "telefono"@it .

:MobilityStation  a      owl:Class ;
        rdfs:comment     "Un Luogo caratterizzato dalla presenza di servizi di mobilità. Può includere altre Stazioni di Mobilità dedicate a specifici servizi per la mobilità (Stazioni di Sharing, Stazioni di Ricarica, etc.)."@it , "A Place characterized by mobility services. A Mobility Station can include more specific Mobility Station (Sharing Station, Charging Station, ecc.)."@en ;
        rdfs:label       "Stazione di Mobilità"@it , "Mobility Station"@en ;
        rdfs:subClassOf  <http://schema.org/Place> .

<http://www.w3.org/ns/locn#registeredAddress>
        a           owl:ObjectProperty ;
        rdfs:label  "sede legale"@it , "registered address"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare una legal:LegalEntity (Entità Legale) al locn:Address (Indirizzo) della sua sede legale."@it .

<http://purl.org/dc/terms/bibliographicCitation>
        a       owl:AnnotationProperty .

<http://schema.org/availableChannel>
        a           owl:ObjectProperty ;
        rdfs:label  "canale disponibile"@it , "available channel"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:Service (Servizio) ad uno schema:ServiceChannel (Canale del Servizio)."@it .

<http://purl.org/dc/terms/Location>
        a           owl:Class ;
        rdfs:label  "Location"@en , "Localizzazione"@it ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per descrivere la localizzazione di uno schema:Place (Luogo)."@it .

<http://www.w3.org/ns/locn#postCode>
        a           owl:DatatypeProperty ;
        rdfs:label  "post code"@en , "codice postale"@it .

<http://schema.org/hoursAvailable>
        a           owl:ObjectProperty ;
        rdfs:label  "orari disponibili"@it , "hours available"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Associa un Servizio alla Specifica Orari Apertura che descrive gli orari in cui è reso disponibile."@it .

<http://purl.org/dc/elements/1.1/contributor>
        a       owl:AnnotationProperty .

:madeAvailableBy  a    owl:ObjectProperty ;
        rdfs:comment   "Associates a Service Resource with the Service making it available."@en , "Associa una Risorsa del Servizio al Servizio che la rende disponibile."@it ;
        rdfs:domain    :ServiceResource ;
        rdfs:label     "resa disponibile da"@it , "made available by"@en ;
        rdfs:range     <http://schema.org/Service> ;
        owl:inverseOf  :makesAvailable .

:sessionEnabledBy  a  owl:ObjectProperty ;
        rdfs:comment  "Associates a Usage Session with the Service enabling it."@en , "Associa una Sessione di Utilizzo al Servizio che la abilita."@it ;
        rdfs:domain   :UsageSession ;
        rdfs:label    "sessione abilitata da"@it , "session enabled by"@en ;
        rdfs:range    <http://schema.org/Service> .

<http://schema.org/availableLanguage>
        a           owl:DatatypeProperty ;
        rdfs:label  "lingua disponibile"@it , "available language"@en .

<http://schema.org/Place>
        a           owl:Class ;
        rdfs:label  "Place"@en , "Luogo"@it .

<http://schema.org/openingHoursSpecification>
        a           owl:ObjectProperty ;
        rdfs:label  "specifica orari di apertura"@it , "opening hours specification"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usata per associare uno schema:Place (Luogo) alla sua schema:OpeningHoursSpecification (Specifica Orari di Apertura)."@it .

<http://www.w3.org/ns/sosa/ObservableProperty>
        a           owl:Class ;
        rdfs:label  "Proprietà Campionabile"@it , "Observable Property"@en .

:ServicePrivateUser  a   owl:Class ;
        rdfs:comment     "Una persona fisica utilizzatore di un servizio."@it , "A natural person using a service."@en ;
        rdfs:label       "Utente Privato del Servizio"@it , "Service Private User"@en ;
        rdfs:subClassOf  :ServiceUser , <http://schema.org/Person> .

:enablesSession  a     owl:ObjectProperty ;
        rdfs:comment   "Associates a Service with a Usage Session enabled by the service."@en , "Associa un Servizio ad una Sessione di Utilizzo abilitata dal Servizio."@it ;
        rdfs:domain    <http://schema.org/Service> ;
        rdfs:label     "enables session"@en , "abilita sessione"@it ;
        rdfs:range     :UsageSession ;
        owl:inverseOf  :sessionEnabledBy .

<http://purl.org/dc/elements/1.1/rights>
        a       owl:AnnotationProperty .

<http://purl.org/dc/elements/1.1/title>
        a       owl:AnnotationProperty .

<http://schema.org/birthDate>
        a           owl:DatatypeProperty ;
        rdfs:label  "data di nascita"@it , "birth date"@en .

<http://www.w3.org/ns/locn#Address>
        a           owl:Class ;
        rdfs:label  "Indirizzo"@it , "Address"@en ;
        <http://purl.org/vocab/vann/usageNote>
                "Usato per definire l'indirizzo associato ad una dcterms:Location, ad una schema:Person (Persona) o la sede legale di una legal:LegalEntity (Entità Legale). Nel caso di luoghi in Italia può essere utilizzato il profilo italiano (https://w3id.org/italia/onto/CLV/) allineato con locn:Address."@it .

:birthYear  a               owl:DatatypeProperty ;
        rdfs:comment        "Associates a person with her/his birth year."@it , "Associa una persona al suo anno di nascita."@it ;
        rdfs:domain         <http://schema.org/Person> ;
        rdfs:label          "birth year"@en , "anno di nascita"@it ;
        rdfs:range          xsd:int ;
        rdfs:subPropertyOf  <http://schema.org/birthDate> .
