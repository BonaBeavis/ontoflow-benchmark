@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix : <http://www.w3.org/1999/xhtml> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix bibo: <http://purl.org/ontology/bibo/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix ev: <http://purl.org/NET/c4dm/event.owl#> .
@prefix qb: <http://purl.org/linked-data/cube#> .
@prefix vann: <http://purl.org/vocab/vann/> .
@prefix ci: <https://privatealpha.com/ontology/content-inventory/1#> .

<https://doriantaylor.com/person/dorian-taylor#me>
    foaf:name "Dorian Taylor"@en .

<https://privatealpha.com/ontology/content-inventory/1>
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Five-number_summary>, <http://en.wikipedia.org/wiki/Mean>, <http://en.wikipedia.org/wiki/Standard_deviation>, <http://publishing-statistical-data.googlecode.com/svn/trunk/specs/src/main/html/cube.html#ref_qb_DataStructureDefinition>, <http://publishing-statistical-data.googlecode.com/svn/trunk/specs/src/main/html/cube.html#ref_qb_DimensionProperty>, <http://publishing-statistical-data.googlecode.com/svn/trunk/specs/src/main/html/cube.html#ref_qb_MeasureProperty>, <http://publishing-statistical-data.googlecode.com/svn/trunk/specs/src/main/html/cube.html#ref_qb_Observation>, <http://www.w3.org/TR/vocab-data-cube/>, <http://www.w3.org/TR/vocab-data-cube/#ref_qb_DataSet> .

<https://privatealpha.com/ontology/content-inventory/1#>
    dct:created "2012-01-23T11:52:00-08:00"^^xsd:dateTime ;
    dct:creator <http://doriantaylor.com/person/dorian-taylor#me>, <https://doriantaylor.com/person/dorian-taylor#me> ;
    dct:modified "2012-12-11T22:22:00-08:00"^^xsd:dateTime, "2014-02-06T14:10:00-08:00"^^xsd:dateTime, "2015-02-03T14:39:00-08:00"^^xsd:dateTime, "2017-04-06T15:24:00-07:00"^^xsd:dateTime, "2018-10-06T16:23:52Z"^^xsd:dateTime ;
    dct:title "A Content Inventory Vocabulary"@en ;
    bibo:uri <https://privatealpha.com/ontology/content-inventory/1#> ;
    vann:preferredNamespacePrefix "ci"^^xsd:string ;
    a bibo:Webpage, owl:Ontology ;
    <http://www.w3.org/1999/xhtml/vocab#license> <http://creativecommons.org/licenses/by/2.5/ca/> ;
    rdfs:comment "This vocabulary defines a number of concepts peculiar to content strategy which are not accounted for by other vocabularies."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    owl:imports <http://purl.org/NET/c4dm/event.owl#>, <http://purl.org/dc/terms/>, <http://purl.org/linked-data/cube#>, <http://purl.org/ontology/bibo/>, <http://xmlns.com/foaf/0.1/> ;
    owl:versionInfo "0.6" ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://creativecommons.org/licenses/by/2.5/ca/>, <http://doriantaylor.com/person/dorian-taylor#me>, <http://en.wikipedia.org/wiki/Content_strategy>, <http://purl.org/NET/c4dm/event.owl#>, <http://purl.org/dc/terms/>, <http://purl.org/linked-data/cube#>, <http://purl.org/ontology/bibo/>, <http://vocab.org/frbr/core>, <http://vocab.org/frbr/extended>, <http://xmlns.com/foaf/0.1/>, <https://doriantaylor.com/person/dorian-taylor#me> .

ci:Action
    a owl:Class ;
    rdfs:comment "An action, as its name implies, is meant to represent something a person or other agent ought to do to a document."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "Action" ;
    rdfs:subClassOf ev:Event ;
    <https://privatealpha.com/ontology/content-inventory/external> ev:Event .

ci:Merge
    a owl:Class ;
    rdfs:comment "In order to merge a document, we must define the target to which it ought to be merged. This class is identical to an Action, save for such a property."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "Merge" ;
    rdfs:subClassOf ci:Action .

ci:action
    a owl:ObjectProperty ;
    rdfs:comment "use this to signal an action to take with the document in question."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "action" ;
    rdfs:range ci:Action ;
    rdfs:subPropertyOf ev:factor_of ;
    <https://privatealpha.com/ontology/content-inventory/external> ev:factor_of, foaf:Document .

ci:alias
    a owl:ObjectProperty ;
    rdfs:comment "This is an alternate URI for the subject resource. It is simply meant to annotate a resource with another address. It differs from owl:sameAs in that it does not imply that <a> = <b> as well as <b> = <a>."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "alias" .

ci:assumes
    a owl:ObjectProperty ;
    rdfs:comment "The document assumes the audience is familiar with this concept, and may not mention it explicitly."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "assumes" ;
    rdfs:range skos:Concept ;
    rdfs:seeAlso dct:educationLevel ;
    rdfs:subPropertyOf dct:references ;
    <https://privatealpha.com/ontology/content-inventory/external> dct:educationLevel, dct:references, skos:Concept, foaf:Document .

ci:block
    a qb:MeasureProperty ;
    rdfs:comment "A block count is conceptually similar to a word or section count, though it counts the total of elements in the document considered to be text blocks, such as paragraphs, tables, lists and figures. It is suited for document types that have no concept of (semantic) sections, such as HTML. The purpose of this measurement is to provide a sort of ratio to the word count, to glean how well-proportioned the document is."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "blocks" ;
    rdfs:range xsd:nonNegativeInteger ;
    <https://privatealpha.com/ontology/content-inventory/external> qb:Observation, xsd:nonNegativeInteger .

ci:canonical
    a owl:FunctionalProperty, owl:ObjectProperty ;
    rdfs:comment "This is the canonical URI of the subject resource, i.e., the one you always want to publish in content or redirect Web requests to."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "canonical" ;
    rdfs:subPropertyOf owl:sameAs ;
    <https://privatealpha.com/ontology/content-inventory/external> owl:sameAs .

ci:canonical-slug
    a owl:DatatypeProperty, owl:FunctionalProperty ;
    rdfs:comment "This is the canonical slug associated with the resource, and should be populated with the slug which is actually in use."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "canonical-slug" ;
    rdfs:range xsd:string ;
    rdfs:subPropertyOf ci:slug ;
    <https://privatealpha.com/ontology/content-inventory/external> xsd:string .

ci:characters
    a qb:MeasureProperty ;
    rdfs:comment "This indicates the number of characters in a document, with punctuation and the XPath normalize-space function applied. Note this is characters, not bytes."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "characters" ;
    rdfs:range xsd:nonNegativeInteger ;
    rdfs:seeAlso <http://www.w3.org/TR/xpath/#function-normalize-space> ;
    <https://privatealpha.com/ontology/content-inventory/external> qb:Observation, xsd:nonNegativeInteger, <http://www.w3.org/TR/xpath/#function-normalize-space> .

ci:desired-outcome
    a owl:ObjectProperty ;
    rdfs:comment "This property is intended to indicate what the document is supposed to do—what material effect it is supposed to produce. It is intentionally open-ended, and as such can point to something like a skos:Concept, another document, or a literal string of text describing the outcome."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "desired-outcome" ;
    rdfs:subPropertyOf dct:type ;
    <https://privatealpha.com/ontology/content-inventory/external> dct:type, foaf:Document .

ci:document
    a qb:DimensionProperty ;
    rdfs:comment "Document Reference"@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "document" ;
    rdfs:range foaf:Document ;
    <https://privatealpha.com/ontology/content-inventory/external> qb:Observation, foaf:Document .

ci:embed
    a owl:ObjectProperty ;
    rdfs:comment "This property specifies an embedded resource which is visible in the subject's user interface."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "embed" ;
    rdfs:subPropertyOf dct:hasPart ;
    <https://privatealpha.com/ontology/content-inventory/external> dct:hasPart, foaf:Document .

ci:empty
    a bibo:DocumentStatus ;
    rdfs:comment "The document contains no content."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "empty" .

ci:high-quartile
    a qb:MeasureProperty ;
    rdfs:comment "Third Quartile"@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "high-quartile" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Quartile> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Quartile>, qb:Observation, xsd:number .

ci:incomplete
    a bibo:DocumentStatus ;
    rdfs:comment "The document has been started, but is clearly not finished."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "incomplete" .

ci:incorrect
    a bibo:DocumentStatus ;
    rdfs:comment "The content of this document is factually wrong."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "incorrect" .

ci:indegree
    a qb:MeasureProperty ;
    rdfs:comment "The number of links pointing at the specified resource."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "indegree" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Directed_graph#Indegree_and_outdegree> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Directed_graph#Indegree_and_outdegree>, qb:Observation, xsd:number .

ci:introduces
    a owl:ObjectProperty ;
    rdfs:comment "The document defines, describes, or otherwise introduces the audience to this concept."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "introduces" ;
    rdfs:range skos:Concept ;
    rdfs:subPropertyOf ci:mentions ;
    <https://privatealpha.com/ontology/content-inventory/external> skos:Concept, foaf:Document .

ci:keep
    a ci:Action ;
    rdfs:comment "Keep this document to which this is associated; make no changes to it at this time."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "keep" .

ci:landing
    a bibo:DocumentStatus ;
    rdfs:comment "The resource is a landing page from some other medium (e.g. e-mail, television, billboard). This status is a hint to automated systems which would otherwise orphan or retire a landing page with no inbound links."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "landing" .

ci:link
    a owl:ObjectProperty ;
    rdfs:comment "This property specifies a linked resource which is visible in the subject's user interface."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "link" ;
    rdfs:subPropertyOf dct:references ;
    <https://privatealpha.com/ontology/content-inventory/external> dct:references, foaf:Document .

ci:low-quartile
    a qb:MeasureProperty ;
    rdfs:comment "Equivalent to the bottom quarter, or 25th percentile, of the observed data."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "low-quartile" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Quartile> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Quartile>, qb:Observation, xsd:number .

ci:max
    a qb:MeasureProperty ;
    rdfs:comment "Maximum"@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "max" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Sample_maximum> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Sample_maximum>, qb:Observation, xsd:number .

ci:mean
    a qb:MeasureProperty ;
    rdfs:comment "Mean"@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "mean" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Sample_minimum> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Sample_minimum>, qb:Observation, xsd:number .

ci:median
    a qb:MeasureProperty ;
    rdfs:comment "The median of a population "@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "median" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Median> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Median>, qb:Observation, xsd:number .

ci:mentions
    a owl:ObjectProperty ;
    rdfs:comment "The document explicitly mentions this concept."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "mentions" ;
    rdfs:range skos:Concept ;
    rdfs:subPropertyOf dct:references ;
    <https://privatealpha.com/ontology/content-inventory/external> dct:references, skos:Concept, foaf:Document .

ci:min
    a qb:MeasureProperty ;
    rdfs:comment "The smallest observation in the sample."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "min" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Sample_minimum> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Sample_minimum>, qb:Observation, xsd:number .

ci:non-audience
    a owl:ObjectProperty ;
    rdfs:comment "This property complements dct:audience insofar as enabling the author or editor to designate a set of entities who are explicitly not the intended audience of the document."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "non-audience" ;
    rdfs:range dct:AgentClass ;
    rdfs:seeAlso dct:audience ;
    <https://privatealpha.com/ontology/content-inventory/external> dct:AgentClass, dct:audience, foaf:Document .

ci:obsolete
    a bibo:DocumentStatus ;
    rdfs:comment "The content of this document was correct and relevant at one point, but external circumstances have caused it to lapse in relevance or factual accuracy."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "obsolete" .

ci:orphan
    a bibo:DocumentStatus ;
    rdfs:comment "The resource is not explicitly pending or removed from publication, however it has managed to be disconnected from the rest of the site: There is no path to it from a landing page, and it is not a landing page on its own. That is to say that the resource either has no inbound links, or if it does, those links are from other resources that are in the same situation. Documents which are only linked from retired documents are also considered orphans."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "orphan" .

ci:outdegree
    a qb:MeasureProperty ;
    rdfs:comment "The number of links emanating from the specified resource."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "outdegree" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Directed_graph#Indegree_and_outdegree> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Directed_graph#Indegree_and_outdegree>, qb:Observation, xsd:number .

ci:proofread
    a ci:Action ;
    rdfs:comment "Proofread this document."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "proofread" .

ci:retire
    a ci:Action ;
    rdfs:comment "Remove all references to this document and consign it to the archive."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "retire" .

ci:retired
    a bibo:DocumentStatus ;
    rdfs:comment "The document has been explicitly retired by an editor or curator, but still exists in the archive."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "retired" .

ci:revise
    a ci:Action ;
    rdfs:comment "Revise or restructure this document."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "revise" .

ci:rewrite
    a ci:Action ;
    rdfs:comment "Rewrite this document from scratch."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "rewrite" .

ci:sd
    a qb:MeasureProperty ;
    rdfs:comment "Standard Deviation"@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "sd" ;
    rdfs:range xsd:number ;
    rdfs:seeAlso <http://en.wikipedia.org/wiki/Sample_minimum> ;
    <https://privatealpha.com/ontology/content-inventory/external> <http://en.wikipedia.org/wiki/Sample_minimum>, qb:Observation, xsd:number .

ci:sections
    a qb:MeasureProperty ;
    rdfs:comment "For document types that have a concrete representation of sections, this property may be used to capture their sum."@en ;
    rdfs:domain foaf:Document ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "sections" ;
    rdfs:range xsd:nonNegativeInteger ;
    <https://privatealpha.com/ontology/content-inventory/external> xsd:nonNegativeInteger, foaf:Document .

ci:slug
    a owl:DatatypeProperty ;
    rdfs:comment "The slug is a text token which represents either the full path or terminal path segment of an HTTP(S) URL by which a resource can be located. This property is mainly for the purpose of archiving old or alternative URL paths in a content inventory, for such tasks as generating URL rewriting maps."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "slug" ;
    rdfs:range xsd:string ;
    <https://privatealpha.com/ontology/content-inventory/external> xsd:string .

ci:split
    a ci:Action ;
    rdfs:comment "Split this document into multiple pieces."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "split" .

ci:target
    a owl:ObjectProperty ;
    rdfs:comment "Specify the URI of the target resource into which this document should be merged."@en ;
    rdfs:domain ci:Merge ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "target" ;
    rdfs:range foaf:Document ;
    <https://privatealpha.com/ontology/content-inventory/external> foaf:Document .

ci:tentative-merge
    a ci:Merge ;
    rdfs:comment "Merge this document into some other document, though unspecified at this time as to which."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "tentative-merge" .

ci:unavailable
    a bibo:DocumentStatus ;
    rdfs:comment "The resource at the subject address is unavailable for reasons other than explicit retirement, e.g. HTTP 404 or 403, or going out of print."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "unavailable" .

ci:update-metadata
    a ci:Action ;
    rdfs:comment "Update the metadata of this document, such as keywords, audience, etc."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "update-metadata" .

ci:words
    a qb:MeasureProperty ;
    rdfs:comment "This indicates the number of words in a document, similar to the familiar function in a word processor. The exact method of counting words may vary by document type, language etc., and is thus out of scope from this document."@en ;
    rdfs:domain qb:Observation ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/content-inventory/1#> ;
    rdfs:label "words" ;
    rdfs:range xsd:nonNegativeInteger ;
    <https://privatealpha.com/ontology/content-inventory/external> qb:Observation, xsd:nonNegativeInteger .

ci:words-and-blocks
    qb:component [
        qb:dimension ci:document ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:max ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:mean ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:sd ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:sections ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:blocks ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:words ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:characters ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:min ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:low-quartile ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:median ;
        a qb:ComponentSpecification
    ], [
        qb:measure ci:high-quartile ;
        a qb:ComponentSpecification
    ] ;
    a qb:DataStructureDefinition ;
    rdfs:comment "A set of descriptive statistics pertaining to the number of words per block of text in a given document."@en ;
    rdfs:label "words-and-blocks" .

