@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix geom: <http://data.ign.fr/def/geometrie#> .
@prefix vann: <http://purl.org/vocab/vann/> .
@prefix sf: <http://www.opengis.net/ont/sf#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix ngeo: <http://geovocab.org/geometry#> .
@prefix gsp: <http://www.opengis.net/ont/geosparql#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix voaf: <http://purl.org/vocommons/voaf#> .
@prefix ignf: <http://data.ign.fr/def/ignf#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix cc: <http://creativecommons.org/ns#> .

<http://data.ign.fr/def/geometrie> a owl:Ontology ;
	dcterms:description "Ontologie des primitives géométriques représentant la forme et la localisation d'entités topographiques."@fr ;
	dcterms:title "Ontologie des primitives géométriques"@fr ;
	cc:license <http://creativecommons.org/licenses/by/3.0/> ;
	dcterms:creator <http://recherche.ign.fr/labos/cogit/cv.php?prenom=Nathalie&nom=Abadie> , <http://www.eurecom.fr/~atemezin/> ;
	dcterms:contributor <http://www.eurecom.fr/~troncy/> , <http://data.semanticweb.org/person/bernard-vatant> , <http://recherche.ign.fr/labos/cogit/cv.php?prenom=Bénédicte&nom=Bucher> ;
	dcterms:issued "2013-06-11"^^xsd:date ;
	dcterms:modified "2013-10-01"^^xsd:date , "2014-02-28"^^xsd:date , "2014-08-22"^^xsd:date ;
	dcterms:publisher <http://fr.dbpedia.org/resource/Institut_national_de_l%27information_g%C3%A9ographique_et_foresti%C3%A8re> ;
	vann:preferredNamespacePrefix "geom" ;
	vann:preferredNamespaceUri <http://data.ign.fr/def/geometrie#> ;
	dc:rights "Copyright © 2013 IGN" ;
	cc:license <http://www.data.gouv.fr/Licence-Ouverte-Open-Licence> ;
	owl:versionInfo "Version 1.0 - 2013-11-12" .

<http://data.semanticweb.org/person/bernard-vatant> a foaf:Person .

<http://www.eurecom.fr/~atemezin/> a foaf:Person .

<http://www.eurecom.fr/~troncy/> a foaf:Person .

<http://recherche.ign.fr/labos/cogit/cv.php?prenom=Nathalie&nom=Abadie> a foaf:Person .

<http://recherche.ign.fr/labos/cogit/cv.php?prenom=Bénédicte&nom=Bucher> a foaf:Person .

geom:Geometry a owl:Class ;
	rdfs:comment "Primitive géométrique non instanciable, racine de l'ontologie des primitives géométriques. Une géométrie est associée à un système de coordonnées et un seul."@fr ;
	rdfs:label "Géométrie"@fr , "Geometry"@en .

_:node190ebm61ox42 a owl:Restriction ;
	owl:onClass ignf:CoordinatesSystem ;
	owl:onProperty geom:crs ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

geom:Geometry owl:equivalentClass _:node190ebm61ox42 ;
	rdfs:subClassOf ngeo:Geometry , sf:Geometry .

geom:Point a owl:Class ;
	rdfs:comment "Primitive géométrique permettant de représenter une position géographique. Cette position est représentée via des coordonnées X et Y exprimées dans un système de coordonnées donné. Si le système de coordonnées associé au point le permet, ce dernier peut également avoir des coordonnées Z et M."@fr ;
	rdfs:label "Point"@en , "Point"@fr ;
	rdfs:subClassOf geom:Geometry .

_:node190ebm61ox43 a owl:Class .

_:node190ebm61ox44 a owl:Restriction ;
	owl:onDataRange xsd:double ;
	owl:onProperty geom:coordY ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox45 rdf:first _:node190ebm61ox44 ;
	rdf:rest _:node190ebm61ox46 .

_:node190ebm61ox47 a owl:Restriction ;
	owl:onDataRange xsd:double ;
	owl:onProperty geom:coordX ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox46 rdf:first _:node190ebm61ox47 ;
	rdf:rest _:node190ebm61ox48 .

_:node190ebm61ox49 a owl:Restriction ;
	owl:onDataRange xsd:double ;
	owl:onProperty geom:coordZ ;
	owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox48 rdf:first _:node190ebm61ox49 ;
	rdf:rest _:node190ebm61ox50 .

_:node190ebm61ox51 a owl:Restriction ;
	owl:onDataRange xsd:double ;
	owl:onProperty geom:coordM ;
	owl:maxQualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox50 rdf:first _:node190ebm61ox51 ;
	rdf:rest rdf:nil .

_:node190ebm61ox43 owl:intersectionOf _:node190ebm61ox45 .

geom:Point owl:equivalentClass _:node190ebm61ox43 ;
	rdfs:subClassOf sf:Point .

geom:Curve a owl:Class ;
	rdfs:comment "Primitive géométrique représentant une courbe. Elle n'est pas instanciable. Ses spécialisations sont instanciées sous la forme d'une liste ordonnée de points associée à une fonction d'interpolation."@fr ;
	rdfs:label "Courbe"@fr , "Curve"@en ;
	rdfs:subClassOf geom:Geometry , sf:Curve .

geom:Surface a owl:Class ;
	rdfs:comment " Primitive géométrique représentant localement une image continue d'une région d'un plan. Elle n'est pas instanciable. Le contour d'une surface est un ensemble de courbes fermées correspondant à ses frontières."@fr ;
	rdfs:label "Surface"@en , "Surface"@fr ;
	rdfs:subClassOf geom:Geometry , sf:Surface .

geom:Envelope a owl:Class ;
	rdfs:comment " Primitive géométrique représentant l'enveloppe minimale d'une géométrie. Une enveloppe est représentée par deux points: son coin de coordonnées maximales et son coin de coordonnées minimales."@fr ;
	rdfs:label "Envelope"@en , "Enveloppe"@fr .

_:node190ebm61ox52 a owl:Class .

_:node190ebm61ox53 a owl:Restriction ;
	owl:onClass geom:Point ;
	owl:onProperty geom:upperCorner ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox54 rdf:first _:node190ebm61ox53 ;
	rdf:rest _:node190ebm61ox55 .

_:node190ebm61ox56 a owl:Restriction ;
	owl:onClass geom:Point ;
	owl:onProperty geom:lowerCorner ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox55 rdf:first _:node190ebm61ox56 ;
	rdf:rest rdf:nil .

_:node190ebm61ox52 owl:intersectionOf _:node190ebm61ox54 .

geom:Envelope owl:equivalentClass _:node190ebm61ox52 ;
	rdfs:subClassOf geom:Geometry .

geom:Polygon a owl:Class ;
	rdfs:comment "Primitive géométrique spécialisant la primitive Surface. C'est une surface plane définie par une frontière extérieure et zéro ou plusieurs frontières intérieures. Chaque frontière intérieure définit un trou dans le polygone."@fr ;
	rdfs:label "Polygon"@en , "Polygone"@fr ;
	rdfs:subClassOf geom:Surface .

_:node190ebm61ox57 a owl:Restriction ;
	owl:someValuesFrom geom:LinearRing ;
	owl:onProperty geom:exterior .

geom:Polygon owl:equivalentClass _:node190ebm61ox57 .

_:node190ebm61ox58 a owl:Class .

_:node190ebm61ox59 a owl:Restriction ;
	owl:onClass geom:LinearRing ;
	owl:onProperty geom:exterior ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox60 rdf:first _:node190ebm61ox59 ;
	rdf:rest _:node190ebm61ox61 .

_:node190ebm61ox62 a owl:Restriction ;
	owl:someValuesFrom geom:LinearRing ;
	owl:onProperty geom:interior .

_:node190ebm61ox61 rdf:first _:node190ebm61ox62 ;
	rdf:rest rdf:nil .

_:node190ebm61ox58 owl:intersectionOf _:node190ebm61ox60 .

geom:Polygon rdfs:subClassOf _:node190ebm61ox58 , sf:Polygon .

geom:LineString a owl:Class ;
	rdfs:comment "Primitive géométrique spécialisant la primitive abstraite curve. Ses points sont reliés par une fonction d'interpolation linéaire. Chaque paire de points successifs constitue donc un segment de ligne droite."@fr ;
	rdfs:label "Line string"@en , "Polyligne"@fr ;
	rdfs:subClassOf geom:Curve .

_:node190ebm61ox63 a owl:Restriction ;
	owl:someValuesFrom geom:PointsList ;
	owl:onProperty geom:points .

geom:LineString owl:equivalentClass _:node190ebm61ox63 .

_:node190ebm61ox64 a owl:Restriction ;
	owl:onClass geom:PointsList ;
	owl:onProperty geom:points ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

geom:LineString rdfs:subClassOf _:node190ebm61ox64 , sf:LineString .

geom:LinearRing a owl:Class ;
	rdfs:comment "Primitive géométrique spécialisant la primitive Polyligne. C'est une courbe simple et fermée."@fr ;
	rdfs:label "Anneau"@fr , "Linear ring"@en ;
	rdfs:subClassOf geom:LineString .

_:node190ebm61ox65 a owl:Restriction .

_:node190ebm61ox66 a owl:Class .

_:node190ebm61ox67 rdf:first geom:PointsList ;
	rdf:rest _:node190ebm61ox68 .

_:node190ebm61ox69 a owl:Restriction ;
	owl:onClass geom:Point ;
	owl:onProperty geom:firstAndLast ;
	owl:qualifiedCardinality "1"^^xsd:nonNegativeInteger .

_:node190ebm61ox68 rdf:first _:node190ebm61ox69 ;
	rdf:rest rdf:nil .

_:node190ebm61ox66 owl:intersectionOf _:node190ebm61ox67 .

_:node190ebm61ox65 owl:someValuesFrom _:node190ebm61ox66 ;
	owl:onProperty geom:points .

geom:LinearRing owl:equivalentClass _:node190ebm61ox65 ;
	rdfs:subClassOf sf:LinearRing .

geom:Line a owl:Class ;
	rdfs:comment "Primitive géométrique spécialisant la primitive Polyligne et composée de seulement deux points."@fr ;
	rdfs:label "Ligne"@fr , "Line"@en ;
	rdfs:subClassOf geom:LineString .

_:node190ebm61ox70 a owl:Restriction .

_:node190ebm61ox71 a owl:Class .

_:node190ebm61ox72 rdf:first geom:PointsList ;
	rdf:rest _:node190ebm61ox73 .

_:node190ebm61ox74 a owl:Restriction .

_:node190ebm61ox75 a owl:Class .

_:node190ebm61ox76 rdf:first geom:PointsList ;
	rdf:rest _:node190ebm61ox77 .

_:node190ebm61ox78 a owl:Restriction ;
	owl:onProperty rdf:rest ;
	owl:hasValue rdf:nil .

_:node190ebm61ox77 rdf:first _:node190ebm61ox78 ;
	rdf:rest rdf:nil .

_:node190ebm61ox75 owl:intersectionOf _:node190ebm61ox76 .

_:node190ebm61ox74 owl:someValuesFrom _:node190ebm61ox75 ;
	owl:onProperty rdf:rest .

_:node190ebm61ox73 rdf:first _:node190ebm61ox74 ;
	rdf:rest rdf:nil .

_:node190ebm61ox71 owl:intersectionOf _:node190ebm61ox72 .

_:node190ebm61ox70 owl:someValuesFrom _:node190ebm61ox71 ;
	owl:onProperty geom:points .

geom:Line owl:equivalentClass _:node190ebm61ox70 ;
	rdfs:subClassOf sf:Line .

geom:GeometryCollection a owl:Class ;
	rdfs:comment "Collection comprenant un ou plusieurs objets géométriques, sans structure interne. Tous les éléments d'une collection de géométries sont définis dans le même système de coordonnées."@fr ;
	rdfs:label "Collection de géométries"@fr , "Geometry collection"@en ;
	rdfs:subClassOf geom:Geometry , sf:GeometryCollection .

geom:MultiCurve a owl:Class ;
	rdfs:comment "Collection d'objets géométriques de type Courbe. Cette primitive n'est pas instanciable: elle définit des propriétés pour ses sous-classes."@fr ;
	rdfs:label "Multi curve"@en , "Multicourbe"@fr ;
	rdfs:subClassOf geom:GeometryCollection , sf:MultiCurve .

geom:MultiPoint a owl:Class ;
	rdfs:comment "Collection d'objets géométriques de type Point, non connectés et non ordonnés. Un multipoint est dit simple si tous ses points sont différents (i.e. ont des coordonnées différentes)."@fr ;
	rdfs:label "Multi point"@en , "Multipoint"@fr .

_:node190ebm61ox79 a owl:Restriction ;
	owl:someValuesFrom geom:Point ;
	owl:onProperty geom:pointMember .

geom:MultiPoint owl:equivalentClass _:node190ebm61ox79 ;
	rdfs:subClassOf geom:GeometryCollection , sf:MultiPoint .

geom:MultiPolygon a owl:Class ;
	rdfs:comment "Collection d'objets géométriques de type Polygone. C'est une MultiSurface dont tous les éléments sont des Polygones."@fr ;
	rdfs:label "Multi polygon"@en , "Multipolygone"@fr .

_:node190ebm61ox80 a owl:Restriction ;
	owl:someValuesFrom geom:Polygon ;
	owl:onProperty geom:polygonMember .

geom:MultiPolygon owl:equivalentClass _:node190ebm61ox80 ;
	rdfs:subClassOf geom:MultiSurface , sf:MultiPolygon .

geom:MultiLineString a owl:Class ;
	rdfs:comment "Collection d'objets géométriques de type Polyligne. C'est une MultiCourbe dont tous les éléments sont des Polylignes."@fr ;
	rdfs:label "Multi line string"@en , "Multipolyligne"@fr .

_:node190ebm61ox81 a owl:Restriction ;
	owl:someValuesFrom geom:LineString ;
	owl:onProperty geom:lineStringMember .

geom:MultiLineString owl:equivalentClass _:node190ebm61ox81 ;
	rdfs:subClassOf geom:MultiCurve , sf:MultiLineString .

geom:MultiSurface a owl:Class ;
	rdfs:comment "Collection d'objets géométriques de type Surface. Cette primitive n'est pas instanciable: elle définit des propriétés pour ses sous classes."@fr ;
	rdfs:label "Multi surface"@en , "Multisurface"@fr ;
	rdfs:subClassOf geom:GeometryCollection , sf:MultiSurface .

geom:PointsList a owl:Class ;
	rdfs:label "List of points"@en , "Liste de points"@fr ;
	rdfs:subClassOf rdf:List .

_:node190ebm61ox82 a owl:Restriction ;
	owl:allValuesFrom geom:Point ;
	owl:onProperty rdf:first .

geom:PointsList rdfs:subClassOf _:node190ebm61ox82 .

geom:firstAndLast a owl:ObjectProperty ;
	rdfs:comment "Désigne le point servant de point initial et de point final à une liste circulaire de points."@fr ;
	rdfs:domain geom:PointsList ;
	rdfs:label "first and last"@en , "premier et dernier"@fr ;
	rdfs:subPropertyOf rdf:first ;
	rdfs:range geom:Point .

geom:points a owl:ObjectProperty ;
	rdfs:comment "La liste ordonnée des points consitutant une géométrie de type curve."@fr ;
	rdfs:domain geom:Curve ;
	rdfs:label "points"@en , "points"@fr ;
	rdfs:range geom:PointsList .

geom:crs a owl:ObjectProperty ;
	rdfs:comment "Système de coordonnées associé à une primitive géométrique."@fr ;
	rdfs:domain geom:Geometry ;
	rdfs:label "coordinate reference system"@en , "système de coordonnées"@fr ;
	rdfs:range ignf:CRS .

geom:boundary a owl:ObjectProperty ;
	rdfs:comment "Relie un polygone à un anneau décrivant sa frontière."@fr ;
	rdfs:domain geom:Polygon ;
	rdfs:label "frontière"@fr , "boundary"@en ;
	rdfs:range geom:LinearRing .

geom:interior a owl:ObjectProperty ;
	rdfs:comment "Relie un polygone à un anneau décrivant un trou dans sa surface."@fr ;
	rdfs:domain geom:Polygon ;
	rdfs:label "intérieur"@fr , "interior"@en ;
	rdfs:range geom:LinearRing ;
	rdfs:subPropertyOf geom:boundary .

geom:exterior a owl:ObjectProperty ;
	rdfs:comment "Relie un polygone à un anneau décrivant le contour extérieur de sa surface."@fr ;
	rdfs:domain geom:Polygon ;
	rdfs:label "extérieur"@fr , "exterior"@en ;
	rdfs:range geom:LinearRing ;
	rdfs:subPropertyOf geom:boundary .

geom:pointMember a owl:ObjectProperty ;
	rdfs:comment "Relie une géométrie de type multipoint aux points qui la composent."@fr ;
	rdfs:domain geom:MultiPoint ;
	rdfs:label "point membre"@fr , "point member"@en ;
	rdfs:range geom:Point .

geom:lineStringMember a owl:ObjectProperty ;
	rdfs:comment "Relie une géométrie de type multilinestring aux linestrings qui la composent."@fr ;
	rdfs:domain geom:MultiLineString ;
	rdfs:label "polyligne membre"@fr , "line string member"@en ;
	rdfs:range geom:LineString .

geom:polygonMember a owl:ObjectProperty ;
	rdfs:comment "Relie une géométrie de type multipolygones aux polygones qui la composent."@fr ;
	rdfs:domain geom:MultiPolygon ;
	rdfs:label "polygone membre"@fr , "polygon member"@en ;
	rdfs:range geom:Polygon .

geom:geometry a owl:ObjectProperty ;
	rdfs:comment "Primitive géométrique associée à un objet pour représenter sa localisation et éventuellement sa forme."@fr ;
	rdfs:label "a pour géométrie"@fr , "has geometry"@en ;
	rdfs:range geom:Geometry .

geom:centroid a owl:ObjectProperty ;
	rdfs:comment "Centroide d'une géométrie"@fr ;
	rdfs:domain geom:Surface ;
	rdfs:label "centroid"@en , "centroïde"@fr ;
	rdfs:range geom:Point .

geom:envelope a owl:ObjectProperty ;
	rdfs:comment "Rectangle englobant minimal d'une géométrie"@fr ;
	rdfs:domain geom:Geometry ;
	rdfs:label "envelope"@en , "enveloppe"@fr ;
	rdfs:range geom:Envelope .

geom:upperCorner a owl:ObjectProperty ;
	rdfs:comment "Coin d'une enveloppe correspondant aux valeurs de X et Y les plus élevées."@fr ;
	rdfs:domain geom:Envelope ;
	rdfs:label "upper corner"@en , "coin supérieur"@fr ;
	rdfs:range geom:Point .

geom:lowerCorner a owl:ObjectProperty ;
	rdfs:comment "Coin d'une enveloppe correspondant aux valeurs de X et Y les moins élevées."@fr ;
	rdfs:domain geom:Envelope ;
	rdfs:label "lower corner"@en , "coin inférieur"@fr ;
	rdfs:range geom:Point .

rdf:rest a owl:ObjectProperty ;
	rdfs:comment "La suite d'une liste."@fr ;
	rdfs:domain rdf:List ;
	rdfs:label "rest"@en , "rest"@fr ;
	rdfs:range rdf:List .

geom:coordX a owl:DatatypeProperty ;
	rdfs:comment "L'interprétation des coordonnées d'un point dépend du système de coordonnées de référence associé à ce point. La propriété coordX désigne la coordonnée définie par rapport au premier axe du système de coordonnées."@fr ;
	rdfs:domain geom:Point ;
	rdfs:label "x"@fr , "x"@en ;
	rdfs:range xsd:double .

geom:coordY a owl:DatatypeProperty ;
	rdfs:comment "L'interprétation des coordonnées d'un point dépend du système de coordonnées de référence associé à ce point. La propriété coordY désigne la coordonnée définie par rapport au deuxième axe du système de coordonnées."@fr ;
	rdfs:domain geom:Point ;
	rdfs:label "y"@fr , "y"@en ;
	rdfs:range xsd:double .

geom:coordZ a owl:DatatypeProperty ;
	rdfs:comment "L'interprétation des coordonnées d'un point dépend du système de coordonnées de référence associé à ce point. La propriété coordZ désigne la coordonnée définie par rapport au troisième axe du système de coordonnées. La coordonnées Z d'un point représente typiquement, mais pas nécessairement, l'altitude ou la hauteur de ce point."@fr ;
	rdfs:domain geom:Point ;
	rdfs:label "z"@fr , "z"@en ;
	rdfs:range xsd:double .

geom:coordM a owl:DatatypeProperty ;
	rdfs:comment "L'interprétation des coordonnées d'un point dépend du système de coordonnées de référence associé à ce point. La coordonnées M d'un point représente une mesure."@fr ;
	rdfs:domain geom:Point ;
	rdfs:label "m"@fr , "m"@en ;
	rdfs:range xsd:double .

rdf:nil a rdf:List .
